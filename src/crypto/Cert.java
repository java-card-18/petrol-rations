package crypto;

import java.lang.System;
import java.nio.ByteBuffer;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;

// Structure of the certificate:
/*
|   65     |   8   |4 |    1     ||   64    | (bytes)
|PublicKey |ExpDate|ID|DeviceType||Signature| (data)
The Signature length can vary
*/

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * The Certificate class
 */

public class Cert {

    private final byte[] data;
    private byte[] signature; // ASN.1 encoded signature
    private byte[] certificate;
    private BCECPublicKey publicKey;

    public Cert(BCECPublicKey publicKeyToSign, long expiryDate, byte deviceType, int id) {
        this.publicKey = publicKeyToSign;

        data = new byte[CertConstants.DATA_LENGTH];

        byte[] bid = ByteBuffer.allocate(4).putInt(id).array();
        byte[] bexpiryDate = ByteBuffer.allocate(8).putLong(expiryDate).array();
        byte[] bdeviceType = {deviceType};

        // Append byte arrays
        System.arraycopy(publicKey.getQ().getEncoded(false), 0, data, CertConstants.OFFSET_PUBLIC_KEY, CertConstants.PUBLIC_KEY_LENGTH);
        System.arraycopy(bexpiryDate, 0, data, CertConstants.OFFSET_EXPIRY_DATE, CertConstants.EXPIRY_DATE_LENGTH);
        System.arraycopy(bid, 0, data, CertConstants.OFFSET_DEVICE_ID, CertConstants.DEVICE_ID_LENGTH);
        System.arraycopy(bdeviceType, 0, data, CertConstants.OFFSET_DEVICE_TYPE, CertConstants.DEVICE_TYPE_LENGTH);
    }

    public Cert(byte[] certificate, short offset, short length){
        this.certificate = certificate;
        data = new byte[CertConstants.DATA_LENGTH];
        signature = new byte[length - CertConstants.DATA_LENGTH];
        System.arraycopy(certificate, offset , data, 0, CertConstants.DATA_LENGTH);
        System.arraycopy(certificate, offset + CertConstants.OFFSET_SIGNATURE, signature, 0, length - CertConstants.DATA_LENGTH);
    }

    public byte[] getID() {
        byte[] id = new byte[CertConstants.DEVICE_ID_LENGTH];
        System.arraycopy(data, CertConstants.OFFSET_DEVICE_ID, id, 0, CertConstants.DEVICE_ID_LENGTH);
        return id;
    }

    public byte getDeviceType() {
        return data[CertConstants.OFFSET_DEVICE_TYPE];
    }

    public long getExpiryDate(){
        byte[] expDateBytes = new byte[CertConstants.EXPIRY_DATE_LENGTH];
        System.arraycopy(data, CertConstants.PUBLIC_KEY_LENGTH, expDateBytes, 0, CertConstants.EXPIRY_DATE_LENGTH);
        return ByteBuffer.wrap(expDateBytes).getLong();
    }

    public byte[] getSignature(){
        return signature;
    }

    public byte[] toBytes(){
        return certificate;
    }

    public byte[] getPublicKeyBytes(){
        byte[] pubKey = new byte[CertConstants.PUBLIC_KEY_LENGTH];
        System.arraycopy(data, CertConstants.OFFSET_PUBLIC_KEY,
                         pubKey, (short)0, CertConstants.PUBLIC_KEY_LENGTH);
        return pubKey;
    }

    public void generateSignature(BCECPrivateKey privateKey){
        signature = (SignerFactory.getSignerInstance(privateKey)).generateASN1Signature(data);
        generateCertificate();
    }

    public boolean verifySignature(BCECPublicKey publicKey) {
        return (SignerFactory.getVerifierInstance(publicKey)).verifyASN1Signature(data, signature, (short) 0);
    }

    private void generateCertificate(){
        certificate = new byte[CertConstants.DATA_LENGTH + signature.length];
        System.arraycopy(data, 0 , certificate, 0, CertConstants.DATA_LENGTH);
        System.arraycopy(signature, 0, certificate, CertConstants.DATA_LENGTH , signature.length);
    }
}
