package crypto;

import org.bouncycastle.crypto.signers.DSAKCalculator;
import org.bouncycastle.crypto.signers.ECDSASigner;

import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * Extension on the ECDSASigner to work with ASN.1 encodings
 */
public class PetrolCardSigner extends ECDSASigner {

    MessageDigest hasher;

    public PetrolCardSigner(DSAKCalculator var1) {
        super(var1);
        try {
            hasher = MessageDigest.getInstance("SHA-1");
        } catch (Exception e) {
            System.out.println("Error initializing PetrolCardSigner instance");
            System.out.println(e.getMessage());
        }
    }

    @Override
    public boolean verifySignature(byte[] var1, BigInteger var2, BigInteger var3) {
        // Not allowed in the signer
        return false;
    }

    @Override
    public BigInteger[] generateSignature(byte[] var1) {
        return super.generateSignature(hasher.digest(var1));
    }

    public byte[] generateASN1Signature(byte[] data) {
        BigInteger[] signature = generateSignature(data);

        byte[] r = signature[0].toByteArray();
        byte[] s = signature[1].toByteArray();

        short remainingLength = (short)( 4 + r.length + s.length);
        byte[] returnArray = new byte[2 + remainingLength];
        returnArray[0] = (byte) 0x30;
        returnArray[1] = (byte) remainingLength;
        returnArray[2] = (byte) 0x02;
        returnArray[3] = (byte) r.length;

        System.arraycopy(r, (short)0, returnArray, (short)4, (short) r.length);
        returnArray[4 + r.length] = (byte) 0x02;
        returnArray[4 + r.length + 1] = (byte) s.length;
        System.arraycopy(s, (short)0, returnArray, (short)4 + r.length + 2, (short) s.length);

        return returnArray;
    }
}
