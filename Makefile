# Path to the Java Card Development Kit
JC_HOME=util/java_card_kit-2_2_1

# Version of JCardSim to use;
JCARDSIM=jcardsim-3.0.4-SNAPSHOT

# Bouncy Castle
BOUNCY_CASTLE_HOME=util/bcprov-jdk15on-168

# Beware that only JCardSim-3.0.4-SNAPSHOT.jar includes the classes
# AIDUtil and CardTerminalSimulator, so some of the code samples on
# https://jcardsim.org/docs do not work with older versions
#    JCARDSIM=jcardsim-2.2.1-all
#    JCARDSIM=jcardsim-2.2.2-all

# Classpath for JavaCard code, ie the smartcard applet; this includes
# way more than is probably needed
JC_CLASSPATH=${JC_HOME}/lib/apdutool.jar:${JC_HOME}/lib/apduio.jar:${JC_HOME}/lib/converter.jar:${JC_HOME}/lib/jcwde.jar:${JC_HOME}/lib/scriptgen.jar:${JC_HOME}/lib/offcardverifier.jar:${JC_HOME}/lib/api.jar:${JC_HOME}/lib/installer.jar:${JC_HOME}/lib/capdump.jar:${JC_HOME}/samples/classes:${CLASSPATH}

# compile everything
all: petrolCard chargingTerminal pumpTerminal personalisationTerminal

# things to run:
# runChargingTerminal
# runPumpTerminal
# runPersonalisationTerminal
# runTestCardCharging
# runTestCardPersonalisation

# CARD

petrolCard: bin/PetrolCard.class 

bin/PetrolCard.class: src/card/PetrolCard.java
	javac -d bin -cp ${JC_CLASSPATH}:src src/card/PetrolCard.java src/card/*.java


# CHARGING TERMINAL

chargingTerminal: bin/chargingTerminal/ChargingTerminal.class

bin/chargingTerminal/ChargingTerminal.class: src/chargingTerminal/ChargingTerminal.java
	javac -d bin -cp ${JC_HOME}:${BOUNCY_CASTLE_HOME}.jar:util/jcardsim/${JCARDSIM}.jar:bin src/chargingTerminal/ChargingTerminal.java src/*/*.java

runChargingTerminal: 
	java -cp util/jcardsim/${JCARDSIM}.jar:${BOUNCY_CASTLE_HOME}.jar:bin chargingTerminal.ChargingTerminal


# PUMP TERMINAL

pumpTerminal: bin/pumpTerminal/PumpTerminal.class

bin/pumpTerminal/PumpTerminal.class: src/pumpTerminal/PumpTerminal.java
	javac -d bin -cp ${JC_HOME}:util/jcardsim/${JCARDSIM}.jar:${BOUNCY_CASTLE_HOME}.jar:bin src/pumpTerminal/PumpTerminal.java src/*/*.java

runPumpTerminal: 
	java -cp util/jcardsim/${JCARDSIM}.jar:${BOUNCY_CASTLE_HOME}.jar:bin pumpTerminal.PumpTerminal


# PERSONALISATION TERMINAL

personalisationTerminal: bin/personalisationTerminal/PersonalisationTerminal.class

bin/personalisationTerminal/PersonalisationTerminal.class: src/personalisationTerminal/PersonalisationTerminal.java
	javac -d bin -cp ${JC_HOME}:util/jcardsim/${JCARDSIM}.jar:${BOUNCY_CASTLE_HOME}.jar:bin src/personalisationTerminal/PersonalisationTerminal.java src/*/*.java

runPersonalisationTerminal: 
	java -cp util/jcardsim/${JCARDSIM}.jar:${BOUNCY_CASTLE_HOME}.jar:bin personalisationTerminal.PersonalisationTerminal

clean:
	rm -rf bin/*
