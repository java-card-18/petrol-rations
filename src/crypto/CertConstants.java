package crypto;
/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * The Constants
 */
public class CertConstants {

    public static final short PUBLIC_KEY_LENGTH = 65;
    public static final short EXPIRY_DATE_LENGTH = 8;
    public static final short DEVICE_ID_LENGTH = 4;
    public static final short CARD_ID_LENGTH = 4;
    public static final short DEVICE_TYPE_LENGTH = 1;

    public static short DATA_LENGTH = (short) (
            PUBLIC_KEY_LENGTH + EXPIRY_DATE_LENGTH + DEVICE_ID_LENGTH + DEVICE_TYPE_LENGTH
    );

    public static final short OFFSET_PUBLIC_KEY = 0;
    public static final short OFFSET_EXPIRY_DATE = (short) (OFFSET_PUBLIC_KEY + PUBLIC_KEY_LENGTH);
    public static final short OFFSET_DEVICE_ID = (short) (OFFSET_EXPIRY_DATE + EXPIRY_DATE_LENGTH);
    public static final short OFFSET_DEVICE_TYPE = (short) (OFFSET_DEVICE_ID + DEVICE_ID_LENGTH);
    public static final short OFFSET_SIGNATURE = (short) (OFFSET_DEVICE_TYPE + DEVICE_TYPE_LENGTH);

    public static final short SEQUENCE_NUMBER_LENGTH = 4;
    public static final short CARD_BALANCE_LENGTH = 2;
    public static final short NONCE_LENGTH = 4;
    public static final short MAC_LENGTH = 16;
    public static final short NUMBER_OF_CHARGES_LENGTH = 2;
    public static final short MAX_SIGNATURE_LENGTH = 72;

    public static final byte DEVICE_TYPE_CARD = 0x10;
    public static final byte DEVICE_TYPE_PERSONALISATION_TERMINAL = 0x01;
    public static final byte DEVICE_TYPE_CHARGING_TERMINAL = 0x03;
    public static final byte DEVICE_TYPE_PUMPING_TERMINAL = 0x04;

    public static final byte CHARGING_ALLOWED = 0x10;
    public static final byte CHARGING_NOT_ALLOWED_UNFINISHED_TRANSACTION = 0x20;
    public static final byte CHARGING_NOT_ALLOWED_STOLEN = 0x30;
    public static final byte CHARGING_NOT_ALLOWED_TOO_SOON = 0x40;

    public static final short CHARGING_ALLOWED_RESPONSE_LENGTH = 1;
    public static final short CHARGING_AMOUNT_LENGTH = 2;

    public static final short OFFSET_CHARGING_ALLOWED_RESPONSE = 0;
    public static final short OFFSET_CHARGING_AMOUNT = CHARGING_ALLOWED_RESPONSE_LENGTH;

    public static final short TIMESTAMP_LENGTH = 8;

    public static short BACKEND_CHARGE_RESPONSE_LENGTH = (short) (
            CHARGING_ALLOWED_RESPONSE_LENGTH + CHARGING_AMOUNT_LENGTH + NUMBER_OF_CHARGES_LENGTH + TIMESTAMP_LENGTH
    );

    public static final short CARD_LOG_SIZE = 1024;
    public static final short MAX_SIGNATURE_SIZE = 72;
    public static final short SINGLE_LOG_SIZE = (short) (CertConstants.MAX_SIGNATURE_SIZE + CertConstants.PUMPING_TRANSACTION_DATA_LENGTH);
    public static final short PUMPING_TRANSACTION_DATA_LENGTH = (short) (
        CARD_BALANCE_LENGTH + DEVICE_ID_LENGTH + CARD_ID_LENGTH + TIMESTAMP_LENGTH
    );
}
