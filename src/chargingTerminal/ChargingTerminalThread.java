package chargingTerminal;

import card.InstructionCodes;
import crypto.CertConstants;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import terminal.BaseTerminalThread;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * The ChargingTerminalThread
 */
public class ChargingTerminalThread extends BaseTerminalThread {

    public ChargingTerminalThread(byte[] privateKey, byte[] publicKey, int deviceId, byte[] certificate) {
        super("Charging Terminal", privateKey, publicKey, deviceId, certificate);
    }

    public void run() {
        try {
            print("Start Personalisation");
            BCECPublicKey pubKeyCard = sendGenerateKeysRequest();
            createAndSendCardCertificate(pubKeyCard);

            print("Starting certificate exchange");
            byte[] p = certificateExchange();

            print("Starting mutual auth");
            mutualAuth(p);

            print("Starting key exchange");
            sessionKeyExchange();
            print("Finished Session Key Agreement!");

            // Start charging protocol


            // Send instruction to card to start the charging
            print("Send start instruction");
            byte[] chargeRequestData = this.sendSecure(
                    InstructionCodes.CHARGING_START,
                    new byte[] { 0x00 },
                    (short)(CertConstants.DEVICE_ID_LENGTH + CertConstants.NUMBER_OF_CHARGES_LENGTH + CertConstants.MAX_SIGNATURE_LENGTH)
            );
            // Forward charging request of the card to the backend
            byte[] chargingRequestData = this.backend.handleChargingRequest(chargeRequestData);
            // Forward the backend response to the card
            print("Send backend response to card");
            byte[] chargeResponseData = this.sendSecure(
                    InstructionCodes.CHARGING_BACKEND_RESPONSE,
                    chargingRequestData,
                    (short)(CertConstants.DEVICE_ID_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.DEVICE_ID_LENGTH + CertConstants.TIMESTAMP_LENGTH + CertConstants.MAX_SIGNATURE_LENGTH)
            );

            // Forward signed message of the card to the backend
            short newBalance = this.backend.storeCardChargeTransaction(chargeResponseData);
            print("Charge complete");
            print("New balance: " + newBalance);

        } catch(Exception e) {
            print("Error in charging protocol");
            print(e.getMessage());
        }
    }
}
