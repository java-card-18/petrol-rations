package card;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * The instruction codes for the Card
 */
public final class InstructionCodes {

    private InstructionCodes() {
        // Don't instantiate this class
    }

    // Personalization protocol
    public static final byte GENERATE_KEYS = 0x01;
    public static final byte STORE_CERTIFICATE_AND_ISSUE = 0x02;
    public static final byte REQUEST_LOGS = 0x03;

    // Authentication protocol
    public static final byte CERTIFICATE_EXCHANGE = 0x10;
    public static final byte MUTUAL_AUTHENTICATION = 0x11;
    public static final byte ESTABLISH_SESSION_KEY = 0x12;

    public static final byte AUTH_VALID_CERTIFICATE = 0x13;

    public static final byte CHARGING_START = 0x30;
    public static final byte CHARGING_BACKEND_RESPONSE = 0x31;

    public static final byte PUMP_REQUEST_BALANCE = 0x40;
    public static final byte PUMP_START_TRANSACTION = 0x41;

    public static boolean isAuthenticationInstruction(byte instruction) {
        return instruction >= CERTIFICATE_EXCHANGE && instruction <= ESTABLISH_SESSION_KEY;
    }

    public static boolean isPersonalizationInstruction(byte instruction) {
        return instruction >= GENERATE_KEYS && instruction <= REQUEST_LOGS;
    }

    public static boolean isPumpingInstruction(byte instruction) {
        return instruction >= PUMP_REQUEST_BALANCE && instruction <= PUMP_START_TRANSACTION;
    }
    
    public static boolean isChargingInstruction(byte instruction) {
        return instruction >= CHARGING_START && instruction <= CHARGING_BACKEND_RESPONSE;
    }
}
