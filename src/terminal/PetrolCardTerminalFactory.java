package terminal;

import card.PetrolCard;
import com.licel.jcardsim.smartcardio.CardSimulator;
import com.licel.jcardsim.smartcardio.CardTerminalSimulator;
import javacard.framework.AID;

import javax.smartcardio.*;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * The PetrolCard TerminalFactory. Starts the simulator, and starts the terminal threads
 */
public class PetrolCardTerminalFactory {

    private final String terminalName;

    static final byte[] PETROLCARD_AID = { (byte) 0x3B, (byte) 0x29,
            (byte) 0x63, (byte) 0x61, (byte) 0x6C, (byte) 0x63, (byte) 0x01 };

    static final CommandAPDU SELECT_APDU = new CommandAPDU(
            (byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x00, PETROLCARD_AID);

    public PetrolCardTerminalFactory(String terminalName) {
        this.terminalName = terminalName;
    }

    public CardChannel buildTerminal() throws Exception {
        // Obtain a CardTerminal
        CardTerminals cardTerminals = CardTerminalSimulator.terminals(terminalName);
        CardTerminal terminal1 = cardTerminals.getTerminal(terminalName);

        // Create simulator and install applet
        CardSimulator simulator = new CardSimulator();
        AID cardAppletAID = new AID(PETROLCARD_AID, (byte) 0, (byte) 7);
        simulator.installApplet(cardAppletAID, PetrolCard.class);

        // Insert Card into terminal1
        simulator.assignToTerminal(terminal1);

        Card card = terminal1.connect("*");

        CardChannel applet = card.getBasicChannel();
        ResponseAPDU resp = applet.transmit(SELECT_APDU);
        if (resp.getSW() != 0x9000) {
            throw new Exception("Select failed");
        }
        return applet;
    }
}
