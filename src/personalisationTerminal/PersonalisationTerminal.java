package personalisationTerminal;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * The personalisationTerminal base class
 */
public class PersonalisationTerminal{

	private static byte[] privateKey = {
			(byte) 0x41,(byte) 0xCD,(byte) 0x34,(byte) 0xD9,(byte) 0x0B,(byte) 0x3E,(byte) 0x41,(byte) 0x4E,
			(byte) 0x22,(byte) 0xD2,(byte) 0x78,(byte) 0x87,(byte) 0xF2,(byte) 0x2D,(byte) 0x40,(byte) 0xCB,
			(byte) 0x68,(byte) 0x65,(byte) 0x01,(byte) 0x19,(byte) 0x05,(byte) 0xB6,(byte) 0xBE,(byte) 0x24,
			(byte) 0xF5,(byte) 0xA6,(byte) 0x00,(byte) 0xC1,(byte) 0x8D,(byte) 0x98,(byte) 0xC0,(byte) 0x02,
	};

	private static byte[] publicKey = {
			(byte) 0x04, // formatting byte
			(byte) 0xF9,(byte) 0xE5,(byte) 0x18,(byte) 0xF0,(byte) 0x37,(byte) 0x75,(byte) 0xD4,(byte) 0x08,
			(byte) 0x1F,(byte) 0x7B,(byte) 0x49,(byte) 0x2D,(byte) 0xA1,(byte) 0xA9,(byte) 0x4F,(byte) 0xE5,
			(byte) 0x37,(byte) 0x1A,(byte) 0xB9,(byte) 0xEC,(byte) 0x2B,(byte) 0xDC,(byte) 0x23,(byte) 0x72,
			(byte) 0x5A,(byte) 0x1C,(byte) 0xBC,(byte) 0x3B,(byte) 0x04,(byte) 0x99,(byte) 0x8E,(byte) 0x52,
			(byte) 0x84,(byte) 0x75,(byte) 0xA2,(byte) 0xAA,(byte) 0x25,(byte) 0xBF,(byte) 0xDB,(byte) 0xC9,
			(byte) 0xC2,(byte) 0x40,(byte) 0x46,(byte) 0xD6,(byte) 0x37,(byte) 0x6A,(byte) 0xA4,(byte) 0xB9,
			(byte) 0xDB,(byte) 0x42,(byte) 0xF0,(byte) 0x93,(byte) 0x6C,(byte) 0x52,(byte) 0x86,(byte) 0x1A,
			(byte) 0xA6,(byte) 0xE4,(byte) 0xDE,(byte) 0x7B,(byte) 0x5D,(byte) 0x3E,(byte) 0xF0,(byte) 0x33,
	};

	private static int terminalId = 42;
	
	private byte[] certificate = {
			// Public key
			(byte) 0x04, // formatting Byte
			(byte) 0xF9,(byte) 0xE5,(byte) 0x18,(byte) 0xF0,(byte) 0x37,(byte) 0x75,(byte) 0xD4,(byte) 0x08,
			(byte) 0x1F,(byte) 0x7B,(byte) 0x49,(byte) 0x2D,(byte) 0xA1,(byte) 0xA9,(byte) 0x4F,(byte) 0xE5,
			(byte) 0x37,(byte) 0x1A,(byte) 0xB9,(byte) 0xEC,(byte) 0x2B,(byte) 0xDC,(byte) 0x23,(byte) 0x72,
			(byte) 0x5A,(byte) 0x1C,(byte) 0xBC,(byte) 0x3B,(byte) 0x04,(byte) 0x99,(byte) 0x8E,(byte) 0x52,
			(byte) 0x84,(byte) 0x75,(byte) 0xA2,(byte) 0xAA,(byte) 0x25,(byte) 0xBF,(byte) 0xDB,(byte) 0xC9,
			(byte) 0xC2,(byte) 0x40,(byte) 0x46,(byte) 0xD6,(byte) 0x37,(byte) 0x6A,(byte) 0xA4,(byte) 0xB9,
			(byte) 0xDB,(byte) 0x42,(byte) 0xF0,(byte) 0x93,(byte) 0x6C,(byte) 0x52,(byte) 0x86,(byte) 0x1A,
			(byte) 0xA6,(byte) 0xE4,(byte) 0xDE,(byte) 0x7B,(byte) 0x5D,(byte) 0x3E,(byte) 0xF0,(byte) 0x33,
			// Expiry date
			(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x62,(byte) 0x59,(byte) 0xB3,(byte) 0x93,
			// Device Id
			(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x2A,
			// Device Type
			(byte) 0x01,
			// ASN.1 encoded signature: 0x30 b1 0x02 b2 (vr) 0x02 b3 (vs)
			(byte) 0x30,(byte) 0x45,
			// R
			(byte) 0x02,(byte) 0x20,
			(byte) 0x5D,(byte) 0x10,(byte) 0x46,(byte) 0x4F,(byte) 0xD5,(byte) 0xFF,(byte) 0x30,(byte) 0x4D,
			(byte) 0xA0,(byte) 0x8D,(byte) 0xEE,(byte) 0x4E,(byte) 0x91,(byte) 0xDB,(byte) 0xD9,(byte) 0xD5,
			(byte) 0x54,(byte) 0xB4,(byte) 0x38,(byte) 0xA4,(byte) 0x6A,(byte) 0x7D,(byte) 0xCD,(byte) 0xC4,
			(byte) 0x9A,(byte) 0xE0,(byte) 0xF7,(byte) 0x8E,(byte) 0xA2,(byte) 0xDF,(byte) 0xA1,(byte) 0x03,
			// S
			(byte) 0x02,(byte) 0x21,
			(byte) 0x00,(byte) 0x9A,(byte) 0xEE,(byte) 0x98,(byte) 0x05,(byte) 0x78,(byte) 0xDF,(byte) 0x3A,
			(byte) 0x9D,(byte) 0x31,(byte) 0x27,(byte) 0x66,(byte) 0x3C,(byte) 0x3D,(byte) 0xA0,(byte) 0x2B,
			(byte) 0x04,(byte) 0x4E,(byte) 0xF6,(byte) 0x2B,(byte) 0xC9,(byte) 0xBF,(byte) 0xD3,(byte) 0xDB,
			(byte) 0xC7,(byte) 0xA9,(byte) 0x86,(byte) 0x11,(byte) 0x92,(byte) 0x02,(byte) 0x6B,(byte) 0x4C,(byte) 0xCD
	};

    public PersonalisationTerminal() {
		new PersonalisationTerminalThread(privateKey, publicKey, terminalId, certificate).start();
	}

	public static void main(String[] arg) {
		PersonalisationTerminal term = new PersonalisationTerminal();
	}
}
