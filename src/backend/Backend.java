package backend;

import crypto.Cert;
import crypto.CertConstants;
import crypto.PetrolCardSigner;
import crypto.SignerFactory;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;

import java.lang.System;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * The Backend class
 */
public class Backend {

    private static Backend BACKEND_INSTANCE = null;

    private static byte[] BACKEND_PUBLIC_KEY_BYTES = { (byte) 0x04, // Format byte that prepends the actual key
            (byte)0x42, (byte)0x1E, (byte)0x1B, (byte)0x49, (byte)0xF7, (byte)0x10, (byte)0xFB, (byte)0x17,
            (byte)0xA5, (byte)0xB9, (byte)0x81, (byte)0x84, (byte)0x97, (byte)0x08, (byte)0x31, (byte)0x4E,
            (byte)0xA9, (byte)0x26, (byte)0xD1, (byte)0x1A, (byte)0xDE, (byte)0xB5, (byte)0x75, (byte)0x18,
            (byte)0x3C, (byte)0xBC, (byte)0x13, (byte)0x3D, (byte)0x16, (byte)0x95, (byte)0xB4, (byte)0xF6,

            (byte)0xB3, (byte)0x20, (byte)0xC3, (byte)0x04, (byte)0xD6, (byte)0xDF, (byte)0x0B, (byte)0x5C,
            (byte)0xA7, (byte)0xA5, (byte)0x8E, (byte)0x3A, (byte)0x14, (byte)0x34, (byte)0xA4, (byte)0xAA,
            (byte)0x87, (byte)0x64, (byte)0x6E, (byte)0x91, (byte)0xD5, (byte)0x9E, (byte)0xA7, (byte)0xB1,
            (byte)0x95, (byte)0xF7, (byte)0x94, (byte)0x58, (byte)0x06, (byte)0xCF, (byte)0xCE, (byte)0xF6,
    };
    private static byte[] BACKEND_PRIVATE_KEY_BYTES = {
            (byte)0x9F, (byte)0xF0, (byte)0x48, (byte)0x6C, (byte)0x1E, (byte)0x90, (byte)0x57, (byte)0x23,
            (byte)0x91, (byte)0x9A, (byte)0x1C, (byte)0x61, (byte)0x0E, (byte)0x9F, (byte)0x5F, (byte)0xA3,
            (byte)0x2D, (byte)0xCD, (byte)0xD2, (byte)0x95, (byte)0xF2, (byte)0x28, (byte)0x0D, (byte)0x97,
            (byte)0xFB, (byte)0xDD, (byte)0xCD, (byte)0x72, (byte)0x0B, (byte)0x33, (byte)0x82, (byte)0x38,
    };

    private static long CERTIFICATE_EXPIRY_DURATION = 31536000; // 1 year

    private static final String FN_LOG = "log.txt";

    private BCECPrivateKey privateKey;
    private BCECPublicKey publicKey;

    private Backend(){
        ECParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec("secp256r1");
        ECPublicKeySpec pubKeySpec = new ECPublicKeySpec(ecSpec.getCurve().decodePoint(Backend.BACKEND_PUBLIC_KEY_BYTES), ecSpec);
        ECPrivateKeySpec privKeySpec = new ECPrivateKeySpec(new BigInteger(1, Backend.BACKEND_PRIVATE_KEY_BYTES), ecSpec);
        publicKey = new BCECPublicKey("ECDSA", pubKeySpec, BouncyCastleProvider.CONFIGURATION);
        privateKey = new BCECPrivateKey("ECDSA", privKeySpec, BouncyCastleProvider.CONFIGURATION);
    }

    public static Backend getInstance() {
        if (BACKEND_INSTANCE == null) {
            BACKEND_INSTANCE = new Backend();
        }
        return BACKEND_INSTANCE;
    }

    public BCECPublicKey getPublicKey(){
        return publicKey;
    }

    public byte[] generateCertificate(BCECPublicKey publicKeyToSign, byte deviceType){
        long expiryDate = (System.currentTimeMillis() / 1000L) + CERTIFICATE_EXPIRY_DURATION;
        Cert cert = new Cert(publicKeyToSign, expiryDate, deviceType, getFirstAvailableDeviceId(deviceType));
        cert.generateSignature(privateKey);
        return cert.toBytes();
    }

    private int getFirstAvailableDeviceId(byte deviceType) {
        // open file. get last deviceId. Return +=1
        return 44;
    }

    public byte[] handleChargingRequest(byte[] data) throws Exception { // cardId, amount_of_charges, signature
        boolean valid = true; // assume the signature is valid, since we don't completely implement the backend

        if (!valid)
            throw new Exception("Signature not valid");

        byte[] responseData = new byte[
            CertConstants.CHARGING_ALLOWED_RESPONSE_LENGTH +
            CertConstants.CHARGING_AMOUNT_LENGTH +
            CertConstants.NUMBER_OF_CHARGES_LENGTH +
            CertConstants.TIMESTAMP_LENGTH
        ];

        byte[] cardId = Arrays.copyOfRange(data, 0, CertConstants.DEVICE_ID_LENGTH );
        if (isStolen(cardId)) {
            responseData[CertConstants.OFFSET_CHARGING_ALLOWED_RESPONSE] = CertConstants.CHARGING_NOT_ALLOWED_STOLEN;
        }
        else if (hasUnfinishedChargingTransaction(cardId)) {
            responseData[CertConstants.OFFSET_CHARGING_ALLOWED_RESPONSE] = CertConstants.CHARGING_NOT_ALLOWED_UNFINISHED_TRANSACTION;
        }
        else if (isTooSoonToCharge(cardId)) {
            responseData[CertConstants.OFFSET_CHARGING_ALLOWED_RESPONSE] = CertConstants.CHARGING_NOT_ALLOWED_TOO_SOON;
        }
        else{
            responseData[CertConstants.OFFSET_CHARGING_ALLOWED_RESPONSE] = CertConstants.CHARGING_ALLOWED; // Set charging_allowed response
            setShort(responseData, CertConstants.OFFSET_CHARGING_AMOUNT, getAmountToCharge()); // Set amount to charge

            System.arraycopy(
                    data, CertConstants.DEVICE_ID_LENGTH,
                    responseData, CertConstants.CHARGING_ALLOWED_RESPONSE_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH,
                    CertConstants.NUMBER_OF_CHARGES_LENGTH
            ); // Set number of charges

            System.arraycopy(
                    getCurrentTimestamp(), 0,
                    responseData, CertConstants.CHARGING_ALLOWED_RESPONSE_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.NUMBER_OF_CHARGES_LENGTH,
                    CertConstants.TIMESTAMP_LENGTH
            ); // Set timestamp
        }

        PetrolCardSigner signer = SignerFactory.getSignerInstance(privateKey);
        byte[] signature = signer.generateASN1Signature(responseData);
        return combineArrays(responseData, signature);
    }
    public static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b: bytes) {
            sb.append(String.format("%02X ", b));
        }
        return sb.toString();
    }

    // returns new balance
    public short storeCardChargeTransaction(byte[] data){
        writeLog(data.toString());
        return getShort(data,(short)(CertConstants.DEVICE_ID_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH));
    }

    public short getShort(byte[] source, short offset){
        return ByteBuffer.wrap(source, offset, 2).getShort();
    }


    public static void setShort(byte[] destination, short offset, short data) {
        ByteBuffer bb = ByteBuffer.allocate(2);
        byte[] shortArray = bb.putShort(data).array();
        System.arraycopy(shortArray, 0, destination, offset, 2);
    }

    public byte[] getCurrentTimestamp() {
        return ByteBuffer.allocate(Long.BYTES).putLong(System.currentTimeMillis() / 1000L).array();
    }

    private short getAmountToCharge(){
        return 200;
    }

    private boolean isStolen(byte[] cardId) {
        // Assume for now the card has not been stolen
        return false;
    }

    private boolean hasUnfinishedChargingTransaction(byte[] cardId){
        // Assume for now it's all right
        return false;
    }

    private boolean isTooSoonToCharge(byte[] cardId) {
        // Assume for now the card is allowed to charge itself
        return false;
        //List<Charges> charges = JSON.DeSerialize(file);
        //charges.Where(x => x.deviceId == deviceId).OrderBy(x => x.timestamp).First();
        // if cardResponse is null
        // TERMINATE
    }

    private byte[] combineArrays(byte[] array1, byte[] array2) {
        byte[] data = new byte[array1.length + array2.length ];
        System.arraycopy(array1, 0, data, 0, array1.length);
        System.arraycopy(array2, 0, data, array1.length, array2.length);
        return data;
    }

    // Log has format: {Terminal string}#{device ID}:{data of transaction} ; {signature of transaction} | ......
    public void writeLog(String s){

        try{
            System.out.println("[BACKEND]: Appending to log: " + s);
            File file = new File(FN_LOG);
            FileWriter fw = new FileWriter(file, true);
            fw.write(s);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
