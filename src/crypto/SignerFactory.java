package crypto;

import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.signers.DSAKCalculator;
import org.bouncycastle.crypto.signers.HMacDSAKCalculator;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * SignerFactory to get instances of our custom Signer and Verifier, that are initialized with the correct curve.
 */
public class SignerFactory {

    private final static ECParameterSpec curve = ECNamedCurveTable.getParameterSpec("secp256r1");
    private final static ECDomainParameters domain = new ECDomainParameters(curve.getCurve(), curve.getG(), curve.getN());
    private final static DSAKCalculator dsakCalculator = new HMacDSAKCalculator(new SHA1Digest());

    public static PetrolCardSigner getSignerInstance(BCECPrivateKey privateKey) {
        PetrolCardSigner signer = new PetrolCardSigner(dsakCalculator);
        signer.init(true, new ECPrivateKeyParameters(privateKey.getD(), domain));
        return signer;
    }

    public static PetrolCardVerifier getVerifierInstance(BCECPublicKey publicKey) {
        PetrolCardVerifier signer = new PetrolCardVerifier(dsakCalculator);
        signer.init(false, new ECPublicKeyParameters(curve.getCurve().decodePoint(publicKey.getQ().getEncoded(false)), domain));
        return signer;
    }

    public static PetrolCardVerifier getVerifierInstance(byte[] publicKey) {
        ECPublicKeySpec keySpec = new ECPublicKeySpec(curve.getCurve().decodePoint(publicKey), curve);
        return getVerifierInstance(new BCECPublicKey("ECDSA", keySpec, BouncyCastleProvider.CONFIGURATION));
    }
}
