package personalisationTerminal;

import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import terminal.BaseTerminalThread;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * The PersonalisationTerminalThread
 */
public class PersonalisationTerminalThread extends BaseTerminalThread {

    public PersonalisationTerminalThread(byte[] privateKey, byte[] publicKey, int deviceId, byte[] certificate) {
        super("Personalisation Terminal", privateKey, publicKey, deviceId, certificate);
    }

    public void run() {
        try{
            print("Start Personalisation");
            BCECPublicKey pubKeyCard = sendGenerateKeysRequest();
            createAndSendCardCertificate(pubKeyCard);

            print("Starting certificate exchange");
            byte[] p = certificateExchange();

            print("Starting mutual auth");
            mutualAuth(p);

            print("Starting key exchange");
            sessionKeyExchange();
            print("Finished Session Key Agreement!");

            print("Test to check the expiryDate of Card");
            checkCardExpiryDate();

            print("Test to reading all logs from card");
            readLogs();
        }
        catch (Exception e){
            print("Error in Mutual authentication and key exchange protocol");
            print(e.getMessage());
        }
    }
}
