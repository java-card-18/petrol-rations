package card;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * The Card states
 */
public class CardStates {

    private CardStates() {
        // Don't instantiate this class
    }

    public static final byte STATE_INIT = 0x00;
    public static final byte STATE_ISSUED = 0x01;
    public static final byte STATE_DEACTIVATED = 0x02;
}
