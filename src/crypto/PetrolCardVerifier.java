package crypto;

import org.bouncycastle.crypto.signers.DSAKCalculator;
import org.bouncycastle.crypto.signers.ECDSASigner;

import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * Extension on the ECDSASigner to work with ASN.1 encodings
 */
public class PetrolCardVerifier extends ECDSASigner {

    MessageDigest hasher;

    public PetrolCardVerifier(DSAKCalculator var1) {
        super(var1);
        try {
            hasher = MessageDigest.getInstance("SHA-1");
        } catch (Exception e) {
            System.out.println("Error initializing PetrolCardVerifier instance");
            System.out.println(e.getMessage());
        }
    }

    @Override
    public BigInteger[] generateSignature(byte[] var1) {
        // Not allowed in the verifier
        return null;
    }

    @Override
    public boolean verifySignature(byte[] data, BigInteger bigInteger, BigInteger bigInteger1) {
        return super.verifySignature(hasher.digest(data), bigInteger, bigInteger1);
    }

    public boolean verifyASN1Signature(byte[] data, byte[] asn1Signature, short offset) {
        short lengthR = asn1Signature[offset + 3];
        short lengthS = asn1Signature[offset + 4 + lengthR + 1];

        BigInteger r = new BigInteger(asn1Signature, offset + 4, lengthR);
        BigInteger s = new BigInteger(asn1Signature, offset + 4 + lengthR + 2, lengthS);

        return verifySignature(data, r, s);
    }
}
