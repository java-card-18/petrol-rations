package pumpTerminal;

import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import terminal.BaseTerminalThread;
import java.util.Scanner;  // Input scanner
import card.InstructionCodes;
import crypto.CertConstants;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 */
public class PumpTerminalThread extends BaseTerminalThread {

    public PumpTerminalThread(byte[] privateKey, byte[] publicKey, int deviceId, byte[] certificate) {
        super("Pump Terminal", privateKey, publicKey, deviceId, certificate);
    }

    public void run() {
        try {
            print("Start Personalisation");
            BCECPublicKey pubKeyCard = sendGenerateKeysRequest();
            createAndSendCardCertificate(pubKeyCard);

            print("Starting certificate exchange");
            byte[] p = certificateExchange();

            print("Starting mutual auth");
            mutualAuth(p);

            print("Starting key exchange");
            sessionKeyExchange();
            print("Finished Session Key Agreement!");

            print("Starting pumping protocol");
            doPumping();
                

        } catch(Exception e) {
            print("Error in charging protocol");
            print(e.getMessage());
        }
    }

    private void doPumping() throws Exception{
        print("Request balance");
        byte[] cardPumpResponse = sendSecure(InstructionCodes.PUMP_REQUEST_BALANCE, new byte[]{}, CertConstants.CARD_BALANCE_LENGTH);
        print("Card has balance: " + bytesToShort(cardPumpResponse));


        print("Asking user for desired amount");
        // User input
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the desired pump amount: ");
        short toSubtract = sc.nextShort();
        if (toSubtract <= 0){
            print("Please enter an amount > 0");
            return;
        }
        if (checkEnoughBalance(toSubtract, cardPumpResponse)){
            print("Success, card will subtract " + String.valueOf(toSubtract));
            byte[] data = combineArrays(combineArrays(shortToBytes(toSubtract), intToBytes(deviceId)), combineArrays(cardCert.getID(), longToBytes(System.currentTimeMillis())));
            cardPumpResponse = sendSecure(InstructionCodes.PUMP_START_TRANSACTION, data, (short) CertConstants.MAX_SIGNATURE_SIZE);

            logs.add(toHexString(data) + "; " + toHexString(cardPumpResponse));
            print("Activating pump");
        }
        else{
            print("INSUFFICIENT BALANCE");
            return;
        }

        // TEST sending logs to backend
        boolean backendOnline = true;
        if (logs.size() > 0){
            sendLogsToBackend(backendOnline);
        }
    }

    protected boolean checkEnoughBalance(int toSubtract, byte[] balance){
        return (bytesToShort(balance) >= toSubtract);
        }
}
