package card;

import crypto.CertConstants;
import javacard.framework.*;
import javacard.security.*;
import opencrypto.jcmathlib.*;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 */
public class PetrolCard extends Applet implements ISO7816 {

    byte state; // The applet state
    byte[] tmp; // Temporary buffer in RAM
    byte[] outputBuffer; // Temporary buffer in RAM
    byte[] r; // Sequence number in RAM (for data exchange)
    byte[] terminalCert; // Data from the terminal certificate: Constants for offsets are in CertConstants.java
    byte[] nextInstruction;
    short balance;
    byte[] log; // Log of card
    short logIndex;
    RandomData random;

   static byte[] BACKEND_PUBLIC_KEY_BYTES = { (byte) 0x04, // Format byte that prepends the actual key
            (byte)0x42, (byte)0x1E, (byte)0x1B, (byte)0x49, (byte)0xF7, (byte)0x10, (byte)0xFB, (byte)0x17,
            (byte)0xA5, (byte)0xB9, (byte)0x81, (byte)0x84, (byte)0x97, (byte)0x08, (byte)0x31, (byte)0x4E,
            (byte)0xA9, (byte)0x26, (byte)0xD1, (byte)0x1A, (byte)0xDE, (byte)0xB5, (byte)0x75, (byte)0x18,
            (byte)0x3C, (byte)0xBC, (byte)0x13, (byte)0x3D, (byte)0x16, (byte)0x95, (byte)0xB4, (byte)0xF6,

            (byte)0xB3, (byte)0x20, (byte)0xC3, (byte)0x04, (byte)0xD6, (byte)0xDF, (byte)0x0B, (byte)0x5C,
            (byte)0xA7, (byte)0xA5, (byte)0x8E, (byte)0x3A, (byte)0x14, (byte)0x34, (byte)0xA4, (byte)0xAA,
            (byte)0x87, (byte)0x64, (byte)0x6E, (byte)0x91, (byte)0xD5, (byte)0x9E, (byte)0xA7, (byte)0xB1,
            (byte)0x95, (byte)0xF7, (byte)0x94, (byte)0x58, (byte)0x06, (byte)0xCF, (byte)0xCE, (byte)0xF6,
    };
    ECCurve curve;

    KeyPair keyPair = null; // Keypair of the card
    KeyPair keyPairBackend = null; // Keypair of the backend
    KeyPair keyPairTerminal = null; // Keypair of the terminal the card is communicating with
    KeyPair ephemeralKeyPair = null; // Ephemeral Keypair for sessionkey generation

    Signature signer;
    byte[] cardCertificate;
    short cardCertificateLength;

    AESKey sessionKeyK; // AES Session key k (in RAM)
    Signature aesSigner; // For creating MACs

    short numberOfCharges;

    public PetrolCard() {
        tmp = JCSystem.makeTransientByteArray((short) 256, JCSystem.CLEAR_ON_RESET);
        outputBuffer = JCSystem.makeTransientByteArray((short) 256, JCSystem.CLEAR_ON_RESET);
        terminalCert = JCSystem.makeTransientByteArray((short) 160, JCSystem.CLEAR_ON_RESET);
        r = JCSystem.makeTransientByteArray(CertConstants.SEQUENCE_NUMBER_LENGTH, JCSystem.CLEAR_ON_RESET);
        nextInstruction = JCSystem.makeTransientByteArray((byte) 1, JCSystem.CLEAR_ON_RESET);

        state = CardStates.STATE_INIT;

        random = RandomData.getInstance(RandomData.ALG_SECURE_RANDOM );
        sessionKeyK = (AESKey) KeyBuilder.buildKey(KeyBuilder.TYPE_AES_TRANSIENT_RESET, KeyBuilder.LENGTH_AES_128, false);

        keyPair = new KeyPair(
            (ECPublicKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PUBLIC, SecP256r1.KEY_LENGTH,false),
            (ECPrivateKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PRIVATE,SecP256r1.KEY_LENGTH,false)
        );

        keyPairBackend = new KeyPair(
            (ECPublicKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PUBLIC, SecP256r1.KEY_LENGTH,false),
            (ECPrivateKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PRIVATE,SecP256r1.KEY_LENGTH,false)
        );

        keyPairTerminal = new KeyPair(
            (ECPublicKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PUBLIC, SecP256r1.KEY_LENGTH,false),
            (ECPrivateKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PRIVATE,SecP256r1.KEY_LENGTH,false)
        );

        ephemeralKeyPair = new KeyPair(
                (ECPublicKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PUBLIC, SecP256r1.KEY_LENGTH,false),
                (ECPrivateKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PRIVATE,SecP256r1.KEY_LENGTH,false)
        );

        curve = new ECCurve(false, SecP256r1.p, SecP256r1.a, SecP256r1.b, SecP256r1.G, SecP256r1.r);
        signer = Signature.getInstance(Signature.ALG_ECDSA_SHA, false);
        cardCertificate = new byte[(short) 160];
        balance = 0;

        // For convenience, we set the balance manually to 37, just to be able to test the system.
        balance = 37;

        aesSigner = Signature.getInstance(Signature.ALG_AES_MAC_128_NOPAD, false);

        setPublicKeyBackend(BACKEND_PUBLIC_KEY_BYTES, (short) 0, (short) 65);

        logIndex = (short) 0;
        log = new byte[1024];

        numberOfCharges = 0;
    }

    public static void install(byte[] buffer, short offset, byte length) throws SystemException {
        (new PetrolCard()).register();
    }

    public void process(APDU apdu) throws ISOException, APDUException {
        byte[] buffer = apdu.getBuffer();
        byte instruction = buffer[OFFSET_INS];
        short lc = (short)(buffer[OFFSET_LC] & 0x00FF);

        if (selectingApplet()) return; /* Ignore the APDU that selects this applet... */

        short outLength = 0;
        switch(state){
            case CardStates.STATE_INIT:
                personalize(apdu, instruction, lc);
                break;
            case CardStates.STATE_ISSUED:
                ensureInstructionIsExpected(instruction);
                if (InstructionCodes.isPersonalizationInstruction(instruction) ){
                    ensureTerminalDeviceType(CertConstants.DEVICE_TYPE_PERSONALISATION_TERMINAL);
                    ensureAuthentication(apdu, lc);
                    if (instruction == InstructionCodes.REQUEST_LOGS){
                        outLength = sendLogs(apdu, instruction, lc, CertConstants.SEQUENCE_NUMBER_LENGTH);
                        outLength = convertToAuthenticatedMessage(outLength);
                    }
                }
                else if(InstructionCodes.isAuthenticationInstruction(instruction)){
                    outLength = authenticate(apdu, instruction, lc);
                }
                else if(InstructionCodes.isChargingInstruction(instruction) ){
                    ensureTerminalDeviceType(CertConstants.DEVICE_TYPE_CHARGING_TERMINAL);
                    ensureAuthentication(apdu, lc);

                    print("Got a charging instruction");
                    outLength = charge(apdu, instruction, lc, CertConstants.SEQUENCE_NUMBER_LENGTH);
                    outLength = convertToAuthenticatedMessage(outLength);
                }
                else if(InstructionCodes.isPumpingInstruction(instruction) ){
                    ensureTerminalDeviceType(CertConstants.DEVICE_TYPE_PUMPING_TERMINAL);
                    ensureAuthentication(apdu, lc);

                    print("Got a pumping instruction");
                    outLength = pump(apdu, instruction, lc, CertConstants.SEQUENCE_NUMBER_LENGTH);
                    outLength = convertToAuthenticatedMessage(outLength);
                }
                else{
                    ISOException.throwIt(SW_INS_NOT_SUPPORTED);
                }
                break;
            default:
                ISOException.throwIt(SW_COMMAND_NOT_ALLOWED);
        }

        // Generate and send responseAPDU
        short le = apdu.setOutgoing();
        if(outLength != 0) {
            le = outLength;
        }

        apdu.setOutgoingLength(le);
        apdu.sendBytesLong(outputBuffer, (short)0, le);
    }

    /**
     * Method to check if the current APDU is valid and authenticated
     * Part of the 'Exchanging data' protocol
     * @param apdu The apdu
     * @param lc The data length
     * Throws error if not authenticated.
     */
    private void ensureAuthentication(APDU apdu, short lc) {
        readBuffer(apdu, tmp, (short) 0, lc);

        // Verify the sequence number
        incrementByteArray(r);
        if (Util.arrayCompare(
                tmp, (short) 0,
                r, (short) 0,
                CertConstants.SEQUENCE_NUMBER_LENGTH
        ) != (byte) 0 ){
            ISOException.throwIt(SW_DATA_INVALID);
        }

        // Verify the MAC
        if (!aesSigner.verify(
                tmp, (short) 0, (short) (lc - CertConstants.MAC_LENGTH),
                tmp, (short) (lc - CertConstants.MAC_LENGTH), CertConstants.MAC_LENGTH)
        ) {
            ISOException.throwIt(SW_DATA_INVALID);
        }
    }

    /**
     * Method to create an authenticated response
     * Reads dataLen bytes from outputBuffer
     * Overwrites outputBuffer with authenticated message data
     * Part of the 'Exchanging data' protocol
     * @param dataLen Length of data in outputBuffer (from index 0)
     * @return the amount of bytes to send back
     */
    private short convertToAuthenticatedMessage(short dataLen) {
        incrementByteArray(r);

        Util.arrayCopy(outputBuffer, (short) 0, tmp, (short) 0, dataLen);
        Util.arrayFillNonAtomic(outputBuffer, (short) 0, (short) outputBuffer.length, (byte) 0x00);
        Util.arrayCopy(r, (short) 0, outputBuffer, (short) 0, CertConstants.SEQUENCE_NUMBER_LENGTH);
        Util.arrayCopy(tmp, (short) 0, outputBuffer, CertConstants.SEQUENCE_NUMBER_LENGTH, dataLen);

        short paddedDataLength = (short) (dataLen + CertConstants.SEQUENCE_NUMBER_LENGTH);
        short remainder = (short) (paddedDataLength % (short) 16);
        if (remainder > (short) 0)
        {
            paddedDataLength += (short) 16 - remainder;
        }
        aesSigner.sign(outputBuffer, (short) 0, paddedDataLength, outputBuffer, paddedDataLength);

        return (short) (paddedDataLength + CertConstants.MAC_LENGTH);
    }


    /**
     * @return number of bytes of signature output in sigBuff
     */
    private short asymmetricSign(
            byte[] inBuff, short inOffset, short inLength,
            byte[] signatureBuff, short signatureOffset
    ){
        signer.init(keyPair.getPrivate(), Signature.MODE_SIGN);
        return signer.sign(inBuff, inOffset, inLength, signatureBuff, signatureOffset);
    }

    private boolean asymmetricVerify(
            byte[] inBuff, short inOffset, short inLength,
            byte[] signatureBuff, short signatureOffset, short signatureLength,
            ECPublicKey givenPubKey
    ){
        signer.init(givenPubKey, Signature.MODE_VERIFY);
        return signer.verify(inBuff, inOffset, inLength, signatureBuff, signatureOffset, signatureLength);
    }

    /**
     * Method to handle the protocol for the personalization terminal
     * @param apdu The apdu
     * @param instruction The instruction byte
     * @param lc The length of command data
     */
    private void personalize(APDU apdu, byte instruction, short lc) {
        switch (instruction) {
            case InstructionCodes.GENERATE_KEYS:
                keyPair = genEccKeyPair(keyPair);
                ECPublicKey pubKey = (ECPublicKey)keyPair.getPublic();
                pubKey.getW(outputBuffer, (short) 0);
                break;
            case InstructionCodes.STORE_CERTIFICATE_AND_ISSUE:
                readBuffer(apdu, cardCertificate, (short)0, lc);
                cardCertificateLength = (short) ((short) cardCertificate[CertConstants.DATA_LENGTH + 1] + 2 + CertConstants.DATA_LENGTH);
                state = CardStates.STATE_ISSUED;
                print("Card Issued: " + state);
                break;
            default:
                ISOException.throwIt(SW_INS_NOT_SUPPORTED);
                break;
        }
    }

    /**
     * Method to handle the authentication protocol
     * @param apdu The apdu
     * @param instruction The instruction byte
     * @param lc The length of the input data
     */
    private short authenticate(APDU apdu, byte instruction, short lc) {
        short signatureLength;
        switch (instruction) {
            case InstructionCodes.CERTIFICATE_EXCHANGE:
                // Load the certificate into TMP array, and verify its validity and if it's not a card certificate
                readBuffer(apdu, tmp, (short) 0, lc);
                signatureLength = getASN1TotalSignatureLength(tmp, CertConstants.DATA_LENGTH);
                if (!asymmetricVerify(
                        tmp, (short) 0, CertConstants.DATA_LENGTH,
                        tmp, CertConstants.DATA_LENGTH, signatureLength,
                        getPublicKeyBackend())
                    ||
                        tmp[CertConstants.OFFSET_DEVICE_TYPE] == CertConstants.DEVICE_TYPE_CARD
                ){
                    print("Terminal certificate in CERTIFICATE_EXCHANGE is not valid");
                    ISOException.throwIt(SW_DATA_INVALID);
                }

                // Store terminal certificate
                storeTerminalCertificate(tmp, (short) 0, (short) (CertConstants.DATA_LENGTH + signatureLength));
                print("Successfully verified and stored terminal certificate");

                // Generate random Nonce P
                random.generateData(tmp, (short) 0, CertConstants.NONCE_LENGTH);

                // Send back to Terminal: P, CardCertificate
                Util.arrayCopy(
                        tmp, (short) 0,
                        outputBuffer, (short) 0,
                        CertConstants.NONCE_LENGTH
                );
                Util.arrayCopy(
                        cardCertificate, (short) 0,
                        outputBuffer, CertConstants.NONCE_LENGTH,
                        cardCertificateLength
                );
                nextInstruction[0] = InstructionCodes.MUTUAL_AUTHENTICATION;
                return (short) (cardCertificateLength + CertConstants.NONCE_LENGTH);
            case InstructionCodes.MUTUAL_AUTHENTICATION:
                // Assuming that this instruction comes directly after ins CERTIFICATE_EXCHANGE, TMP still has Nonce P
                // Put CardId in front of Nonce P. We need this for verification.
                Util.arrayCopy(tmp, (short) 0, tmp, CertConstants.DEVICE_ID_LENGTH, CertConstants.NONCE_LENGTH);
                Util.arrayCopy(cardCertificate, CertConstants.OFFSET_DEVICE_ID, tmp, (short) 0, CertConstants.DEVICE_ID_LENGTH);
                short dataLength = (short) (CertConstants.DEVICE_ID_LENGTH + CertConstants.NONCE_LENGTH);

                // Get APDU data, and validate if Terminal sent [CardIc, P]
                readBuffer(apdu, tmp, dataLength, lc);
                signatureLength = getASN1TotalSignatureLength(tmp, (short)(dataLength + CertConstants.NONCE_LENGTH));
                if(!asymmetricVerify(
                        tmp, (short) 0, dataLength,
                        tmp, (short)(dataLength + CertConstants.NONCE_LENGTH), signatureLength,
                        getPublicKeyTerminal()
                )) {
                    print("Verification of nonce P failed!");
                    ISOException.throwIt(SW_DATA_INVALID);
                }
                print("Successfully verified nonce P");

                // Set TerminalId, nonce Q in TMP array. Create Signature. Send back to card: [TerminalId, Nonce Q]
                Util.arrayCopy(
                        terminalCert, CertConstants.OFFSET_DEVICE_ID,
                        tmp, CertConstants.DEVICE_ID_LENGTH,
                        CertConstants.DEVICE_ID_LENGTH
                );

                signatureLength = asymmetricSign(
                        tmp, CertConstants.DEVICE_ID_LENGTH, (short) (CertConstants.DEVICE_ID_LENGTH + CertConstants.NONCE_LENGTH),
                        tmp, (short) (CertConstants.DEVICE_ID_LENGTH + CertConstants.DEVICE_ID_LENGTH + CertConstants.NONCE_LENGTH)
                );

                Util.arrayCopy(
                        tmp, (short) (CertConstants.DEVICE_ID_LENGTH + CertConstants.DEVICE_ID_LENGTH + CertConstants.NONCE_LENGTH),
                        outputBuffer, (short) 0,
                        signatureLength
                );
                nextInstruction[0] = InstructionCodes.ESTABLISH_SESSION_KEY;
                return signatureLength;
            case InstructionCodes.ESTABLISH_SESSION_KEY:
                // Validate if Terminal sent Y, [Y]
                readBuffer(apdu, tmp, (short) 0, lc);
                signatureLength = getASN1TotalSignatureLength(tmp, CertConstants.PUBLIC_KEY_LENGTH);

                if(!asymmetricVerify(
                        tmp, (short) 0, CertConstants.PUBLIC_KEY_LENGTH,
                        tmp, CertConstants.PUBLIC_KEY_LENGTH, signatureLength,
                        getPublicKeyTerminal()
                )) {
                    print("Signature of ephemeral public key of terminal is not valid.");
                    ISOException.throwIt(SW_DATA_INVALID);
                }

                // Initialize KeyAgreement based on the public key that the terminal sent us
                ephemeralKeyPair = genEccKeyPair(ephemeralKeyPair);
                KeyAgreement keyAgreement = KeyAgreement.getInstance(KeyAgreement.ALG_EC_SVDP_DH, false);
                keyAgreement.init(ephemeralKeyPair.getPrivate());
                keyAgreement.generateSecret(tmp, (short) 0, CertConstants.PUBLIC_KEY_LENGTH, tmp, (short) (CertConstants.PUBLIC_KEY_LENGTH + signatureLength));

                // Set AES session key using secret shared key data
                sessionKeyK.setKey(tmp, (short) (CertConstants.PUBLIC_KEY_LENGTH + signatureLength));
                sessionKeyK.getKey(tmp, (short) 0);
                print("AES KEY: " + toHexString(tmp, (short) 16));

                // Init MACcer with the new session key
                aesSigner.init(sessionKeyK, Signature.MODE_SIGN);

                // Randomly generate an initial sequence number, starting with 0x00
                random.generateData(r, (short) 1, (short) (CertConstants.SEQUENCE_NUMBER_LENGTH - 1));
                r[0] = (byte) 0;
                Util.arrayCopy(r, (short) 0, outputBuffer, (short) 0, CertConstants.SEQUENCE_NUMBER_LENGTH);
                print("Initial sequence number: " + toHexString(outputBuffer, CertConstants.SEQUENCE_NUMBER_LENGTH));

                // Send R, X, [X] to the terminal
                ((ECPublicKey) ephemeralKeyPair.getPublic()).getW(outputBuffer, CertConstants.SEQUENCE_NUMBER_LENGTH);

                signatureLength = asymmetricSign(
                        outputBuffer, CertConstants.SEQUENCE_NUMBER_LENGTH, CertConstants.PUBLIC_KEY_LENGTH,
                        outputBuffer, (short)(CertConstants.SEQUENCE_NUMBER_LENGTH + CertConstants.PUBLIC_KEY_LENGTH)
                );

                setNextInstructionBasedOnProtocolType();
                return (short) (CertConstants.SEQUENCE_NUMBER_LENGTH + CertConstants.PUBLIC_KEY_LENGTH + signatureLength);
            default:
                ISOException.throwIt(SW_INS_NOT_SUPPORTED);
                break;
        }
        return 0;
    }

    /**
     * Method to handle the protocol for the charging terminal
     * @param apdu The apdu
     * @param instruction The instruction byte
     * @param lc The length of the input data
     * @param dataOffset The offset of the data in the input data array
     */
    private short charge(APDU apdu, byte instruction, short lc, short dataOffset) {
        short signatureLength;
        switch (instruction) {
            case InstructionCodes.CHARGING_START:
                Util.arrayCopy(cardCertificate, CertConstants.OFFSET_DEVICE_ID, outputBuffer, (short) 0, CertConstants.DEVICE_ID_LENGTH);
                Util.setShort(outputBuffer, CertConstants.DEVICE_ID_LENGTH, numberOfCharges);

                signatureLength = asymmetricSign(
                        outputBuffer,
                        (short) 0,
                        (short)(CertConstants.DEVICE_ID_LENGTH + CertConstants.NUMBER_OF_CHARGES_LENGTH),
                        outputBuffer,
                        (short)(CertConstants.DEVICE_ID_LENGTH + CertConstants.NUMBER_OF_CHARGES_LENGTH)
                );
                nextInstruction[0] = InstructionCodes.CHARGING_BACKEND_RESPONSE;
                return (short)( CertConstants.DEVICE_ID_LENGTH + CertConstants.NUMBER_OF_CHARGES_LENGTH + signatureLength);
            case InstructionCodes.CHARGING_BACKEND_RESPONSE:
                if(!asymmetricVerify(
                        tmp, dataOffset, CertConstants.BACKEND_CHARGE_RESPONSE_LENGTH,
                        tmp, (short)(dataOffset + CertConstants.BACKEND_CHARGE_RESPONSE_LENGTH), getASN1TotalSignatureLength(tmp, (short)(dataOffset + CertConstants.BACKEND_CHARGE_RESPONSE_LENGTH)),
                        getPublicKeyBackend())
                ){
                    ISOException.throwIt(SW_DATA_INVALID);
                }

                if(tmp[dataOffset + CertConstants.OFFSET_CHARGING_ALLOWED_RESPONSE] == CertConstants.CHARGING_NOT_ALLOWED_STOLEN){
                    state = CardStates.STATE_DEACTIVATED;
                    ISOException.throwIt(SW_UNKNOWN);
                }
                else if(tmp[dataOffset + CertConstants.OFFSET_CHARGING_ALLOWED_RESPONSE] == CertConstants.CHARGING_NOT_ALLOWED_TOO_SOON){
                    return -1;
                }
                else if(tmp[dataOffset + CertConstants.OFFSET_CHARGING_ALLOWED_RESPONSE] == CertConstants.CHARGING_NOT_ALLOWED_UNFINISHED_TRANSACTION){
                    state = CardStates.STATE_DEACTIVATED;
                    ISOException.throwIt(SW_UNKNOWN);
                }

                // verify if amount_of_charges is equal
                if (numberOfCharges != Util.getShort(tmp, (short) (dataOffset + CertConstants.CHARGING_ALLOWED_RESPONSE_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH))) {
                    // FAILS
                    ISOException.throwIt(SW_CONDITIONS_NOT_SATISFIED);
                }
                // Setup all the data in the outputBuffer
                // card_id, charging_amount, new_balance, terminal_id, timestamp

                short amountToCharge = Util.getShort(tmp, (short)(dataOffset + CertConstants.CHARGING_ALLOWED_RESPONSE_LENGTH));
                if (!increaseBalance(amountToCharge, true)) {
                    ISOException.throwIt(SW_DATA_INVALID);
                }

                // Now we know it's safe to increase the balance, so we can sign the message
                short newBalance = (short) (balance + amountToCharge);
                Util.arrayCopy(cardCertificate, CertConstants.OFFSET_DEVICE_ID, outputBuffer, (short) 0, CertConstants.DEVICE_ID_LENGTH);
                Util.arrayCopy(tmp, (short)(dataOffset + CertConstants.CHARGING_ALLOWED_RESPONSE_LENGTH), outputBuffer, CertConstants.DEVICE_ID_LENGTH, CertConstants.CHARGING_AMOUNT_LENGTH);
                Util.setShort(outputBuffer,(short)(CertConstants.DEVICE_ID_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH), newBalance);
                Util.arrayCopy(
                        terminalCert, CertConstants.OFFSET_DEVICE_ID,
                        outputBuffer, (short)(CertConstants.DEVICE_ID_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH),
                        CertConstants.DEVICE_ID_LENGTH
                );
                Util.arrayCopy(
                        tmp, (short)(dataOffset + CertConstants.CHARGING_ALLOWED_RESPONSE_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.NUMBER_OF_CHARGES_LENGTH),
                        outputBuffer, (short)(CertConstants.DEVICE_ID_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.DEVICE_ID_LENGTH),
                        CertConstants.TIMESTAMP_LENGTH
                );

                // Sign the data in the outputBuffer
                signatureLength = asymmetricSign(
                        outputBuffer, (short) 0, (short)(CertConstants.DEVICE_ID_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.DEVICE_ID_LENGTH + CertConstants.TIMESTAMP_LENGTH),
                        outputBuffer, (short)(CertConstants.DEVICE_ID_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.DEVICE_ID_LENGTH + CertConstants.TIMESTAMP_LENGTH)
                );

                // Store transaction in card log
                appendToLog(outputBuffer, (short)((CertConstants.DEVICE_ID_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.DEVICE_ID_LENGTH + CertConstants.TIMESTAMP_LENGTH) + signatureLength), (short) 0);

                // Now finally we can increase the actual balance
                if (!increaseBalance(Util.getShort(tmp, (short)(dataOffset + CertConstants.CHARGING_ALLOWED_RESPONSE_LENGTH)), false)) {
                    ISOException.throwIt(SW_DATA_INVALID);
                }
                nextInstruction[0] = (byte) 0x00;
                return (short)((CertConstants.DEVICE_ID_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.CHARGING_AMOUNT_LENGTH + CertConstants.DEVICE_ID_LENGTH + CertConstants.TIMESTAMP_LENGTH) + signatureLength);
            default:
                ISOException.throwIt(SW_INS_NOT_SUPPORTED);
                break;
        }
        return 0;
    }

    /**
     * Method to handle the protocol for the pump terminal
     * @param apdu The apdu
     * @param instruction The instruction byte
     * @param lc The length of the input data
     * @param dataOffset The offset of the data in the input data array
     */
    private short pump(APDU apdu, byte instruction, short lc, short dataOffset) {
        switch (instruction) {
            case InstructionCodes.PUMP_REQUEST_BALANCE:
                Util.setShort(outputBuffer, (short) 0, balance);
                nextInstruction[0] = InstructionCodes.PUMP_START_TRANSACTION;
                return CertConstants.CARD_BALANCE_LENGTH;
            case InstructionCodes.PUMP_START_TRANSACTION:
                if (decreaseBalance(Util.getShort(tmp, CertConstants.SEQUENCE_NUMBER_LENGTH))){

                    short signature_length = asymmetricSign(
                            tmp, CertConstants.SEQUENCE_NUMBER_LENGTH, CertConstants.PUMPING_TRANSACTION_DATA_LENGTH,
                            outputBuffer, (short) 0
                    );

                    Util.arrayCopy(
                            outputBuffer, (short) 0,
                            tmp, (short) (CertConstants.SEQUENCE_NUMBER_LENGTH + CertConstants.PUMPING_TRANSACTION_DATA_LENGTH),
                            signature_length
                    );

                    appendToLog(tmp, (short) (signature_length + CertConstants.PUMPING_TRANSACTION_DATA_LENGTH), CertConstants.SEQUENCE_NUMBER_LENGTH );
                    nextInstruction[0] = (byte) 0x00;
                    return signature_length;
                }
                else{
                    ISOException.throwIt(SW_INS_NOT_SUPPORTED);
                }
                break;
            default:
                ISOException.throwIt(SW_INS_NOT_SUPPORTED);
                break;
        }
        return 0;
    }

    private boolean increaseBalance(short amountToIncrease, boolean dryRun) {
        if (amountToIncrease <= 0 || balance < 0){
            ISOException.throwIt(SW_DATA_INVALID);
        }

        if ((Short.MAX_VALUE - amountToIncrease) < balance) {
            print("Amount is too large");
            return false;
        }

        if(!dryRun){
            balance += amountToIncrease;
        }

        return true;
    }

    private boolean decreaseBalance(short amountToDecrease) {
        if (amountToDecrease <= 0 || balance < 0) {
            ISOException.throwIt(SW_DATA_INVALID);
        }

        if (balance < amountToDecrease){
            print("Not enough balance");
            return false;
        }

        balance -= amountToDecrease;
        return true;
    }

    /**
     * Method to handle the protocol for sending logs when requested by terminal
     * @param apdu The apdu
     * @param instruction The instruction byte
     * @param lc The length of the input data
     * @param dataOffset The offset of the data in the input data array
     */
    private short sendLogs(APDU apdu, byte instruction, short lc, short dataOffset) {

        short logIdx = (short) tmp[CertConstants.SEQUENCE_NUMBER_LENGTH];
        print("Got log request for "+ logIdx);

        if (logIdx < (short) 0 || logIdx >=  (short) (CertConstants.CARD_LOG_SIZE / CertConstants.SINGLE_LOG_SIZE) )
        {
            print("Invalid log index");
            Util.arrayFillNonAtomic(outputBuffer, (short) 0, CertConstants.SINGLE_LOG_SIZE, (byte) 0xFF);
            return CertConstants.SINGLE_LOG_SIZE;
        }

        Util.arrayCopy(log, (short) (logIdx * CertConstants.SINGLE_LOG_SIZE), outputBuffer, (short) 0, CertConstants.SINGLE_LOG_SIZE);

        return CertConstants.SINGLE_LOG_SIZE;
    }

    private void ensureTerminalDeviceType(byte requiredDeviceType) {
        if(terminalCert[CertConstants.OFFSET_DEVICE_TYPE] != requiredDeviceType)
            ISOException.throwIt(SW_COMMAND_NOT_ALLOWED);
    }

    private void ensureInstructionIsExpected(byte currentInstruction) {
        if(currentInstruction == InstructionCodes.CERTIFICATE_EXCHANGE){
            // This always is the first instruction, so don't throw an error the first time we call this
            return;
        }
        if (currentInstruction != nextInstruction[0]) {
            ISOException.throwIt(SW_COMMAND_NOT_ALLOWED);
        }
    }

    private void setNextInstructionBasedOnProtocolType() {
        switch (terminalCert[CertConstants.OFFSET_DEVICE_TYPE]) {
            case CertConstants.DEVICE_TYPE_CHARGING_TERMINAL:
                nextInstruction[0] = InstructionCodes.CHARGING_START;
                break;
            case CertConstants.DEVICE_TYPE_PUMPING_TERMINAL:
                nextInstruction[0] = InstructionCodes.PUMP_REQUEST_BALANCE;
                break;
            case CertConstants.DEVICE_TYPE_PERSONALISATION_TERMINAL:
                nextInstruction[0] = InstructionCodes.REQUEST_LOGS;
                break;
            default:
                ISOException.throwIt(SW_DATA_INVALID);
        }
    }

    private void appendToLog(byte[] data, short dataLength, short dataOffset) {

        short byteIndex = (short) (logIndex * ( CertConstants.MAX_SIGNATURE_SIZE + CertConstants.PUMPING_TRANSACTION_DATA_LENGTH ));
        Util.arrayCopy(
                data, dataOffset,
                log, byteIndex,
                dataLength
        );

        logIndex += (short) 1;
        logIndex = (short) (logIndex % (short) (CertConstants.CARD_LOG_SIZE / (CertConstants.MAX_SIGNATURE_SIZE + CertConstants.PUMPING_TRANSACTION_DATA_LENGTH )));

        //print("log: " + toHexString(log));
    }

    /**
    * Copies length bytes of data (starting at
    * OFFSET_CDATA) from apdu to dest
    * (starting at offset).
    *
    * This method will set apdu to incoming.
    *
    * @param apdu the APDU.
    * @param dest destination byte array.
    * @param offset offset into the destination byte array.
    * @param length number of bytes to copy.
    */
    private void readBuffer(APDU apdu, byte[] dest, short offset, short length) {
        byte[] buf = apdu.getBuffer();
        short readCount = apdu.setIncomingAndReceive();
        short i = 0;
        Util.arrayCopy(buf,OFFSET_CDATA,dest,offset,readCount);
        while ((short)(i + readCount) < length) {
            i += readCount;
            offset += readCount;
            readCount = apdu.receiveBytes(OFFSET_CDATA);
            Util.arrayCopy(buf,OFFSET_CDATA,dest,offset,readCount);
        }
    }

    // For debugging only
    // Officially, we cannot use 'new StringBuilder'
    public static String toHexString(byte[] bytes, short length) {
        StringBuilder sb = new StringBuilder();
        short i = 0;
        while (i < length) {
            sb.append(String.format("%02X ", bytes[i]));
            i++;
        }
        return sb.toString();
    }

    public static String toHexString(byte[] bytes) {
        return toHexString(bytes, (short)bytes.length);
    }

    private static void print(String stringToPrint) {
        System.out.println("[CARD]: " + stringToPrint);
    }

    private KeyPair genEccKeyPair(KeyPair keyPairToUse) {
        return curve.newKeyPair_legacy(keyPairToUse);
    }

    private void setPublicKeyTerminal(byte[] pubKeyBytes, short offset, short length) {
        curve.newKeyPair_legacy(keyPairTerminal);
        ((ECPublicKey) keyPairTerminal.getPublic()).setW(pubKeyBytes, offset, length);
    }

    private ECPublicKey getPublicKeyTerminal() {
        return (ECPublicKey) keyPairTerminal.getPublic();
    }

    private void setPublicKeyBackend(byte[] pubKeyBytes, short offset, short length) {
        curve.newKeyPair_legacy(keyPairBackend);
        ((ECPublicKey) keyPairBackend.getPublic()).setW(pubKeyBytes, offset, length);
    }

    private ECPublicKey getPublicKeyBackend() {
        return (ECPublicKey) keyPairBackend.getPublic();
    }

    private void storeTerminalCertificate(byte[] certificateData, short offset, short certificateLength){
        setPublicKeyTerminal(certificateData, offset, CertConstants.PUBLIC_KEY_LENGTH);
        Util.arrayCopy(certificateData, offset, terminalCert, (short) 0, certificateLength);
    }

    private short getASN1TotalSignatureLength(byte[] signatureBuffer, short offset){
        return (short) ((short) signatureBuffer[offset + 1] + 2);
    }

    // Custom function to increase the number of a byte array
    /*
    You can test it using something like this:
    byte[] a = new byte[] {(byte) 0xAB, (byte) 0x00, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
    print(toHexString(a) + " becomes: " + toHexString(incrementByteArray(a)));
    */
    private void incrementByteArray(byte[] data)
    {
        data[data.length-1] = (byte) (data[data.length-1] + (byte) 1);
        if (data.length > 1)
        {
            short i = (short) (data.length-1);
            boolean stop = false;
            while (!stop && i >= 0){
                if (data[i] == (byte) 0){
                    if (i > 0){
                        data[i-1] = (byte) (data[i-1] + (byte) 1);
                    }
                }
                else{
                    stop = true;
                }
                i = (short) (i - 1);
            }
        }
    }
}
