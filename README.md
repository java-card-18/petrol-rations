# petrol-rations
## Code instructions
Here we provide some basic instructions to run our code

To compile our code, use: `make`

To clean the compiled files, use: `make clean`

To run the personalisation terminal, use: `make runPersonalisationTerminal`

To run the pump terminal, use: `make runPumpTerminal`

To run the charge terminal, use: `make runChargingTerminal`

NB, in both the pump and charging terminals, we first run the personalisation protocol to be able to use issued cards. This is because the simulator always forgets the card data. Also, we initialize the card with a small ration, to be able to run the pump protocol.

