package terminal;

import card.InstructionCodes;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import java.math.BigInteger;
import java.security.*;
import java.util.Locale;
import crypto.*;
import backend.Backend;
import org.bouncycastle.crypto.agreement.ECDHBasicAgreement;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.macs.CBCBlockCipherMac;
import org.bouncycastle.crypto.params.KeyParameter;
import java.lang.Math;
import java.util.Arrays;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * @author Steffan Borgers
 * @author Mark ten Klooster
 * @author Mauk Lemmen
 * @author Bart Pleiter
 *
 * The BaseTerminalThread. All other TerminalThreads extend this class.
 */
public abstract class BaseTerminalThread extends Thread {

    protected CardChannel applet;
    protected final Backend backend;
    protected final ECParameterSpec ecSpec;

    // Terminal data
    protected byte[] terminalCert;
    protected final BCECPrivateKey privateKey;
    protected final BCECPublicKey publicKey;
    protected final int deviceId;
    protected final String terminalName;

    protected ArrayList<String> logs;

    // Session data
    protected byte[] sequenceNumber;
    protected byte[] sessionKey;
    protected Cert cardCert; // certificate of the inserted card
    protected CBCBlockCipherMac maccer;

    public BaseTerminalThread(String name, byte[] privateKey, byte[] publicKey, int deviceId, byte[] certificate) {
        try {
            applet = new PetrolCardTerminalFactory(name).buildTerminal();
        } catch(Exception e) {
            applet = null;
            System.out.println("Exception!");
        }

        this.logs = new ArrayList<String>();

        this.terminalCert = certificate;
        this.terminalName = name;
        this.deviceId = deviceId;

        this.backend = Backend.getInstance();

        Security.addProvider(new BouncyCastleProvider());
        this.ecSpec = ECNamedCurveTable.getParameterSpec("secp256r1");

        ECPublicKeySpec pubKeySpec = new ECPublicKeySpec(ecSpec.getCurve().decodePoint(publicKey), ecSpec);
        ECPrivateKeySpec privKeySpec = new ECPrivateKeySpec(new BigInteger(1, privateKey), ecSpec);
        this.publicKey = new BCECPublicKey("ECDSA", pubKeySpec, BouncyCastleProvider.CONFIGURATION);
        this.privateKey = new BCECPrivateKey("ECDSA", privKeySpec, BouncyCastleProvider.CONFIGURATION);

        this.sessionKey = new byte[16];
        this.sequenceNumber = new byte[CertConstants.SEQUENCE_NUMBER_LENGTH];

        this.maccer = new CBCBlockCipherMac(new AESEngine(), 128, null); // 128 bit CBC mode, to match the MAC function of the card
    }

    protected ResponseAPDU sendCommandAPDU(CommandAPDU commandApdu) throws CardException {
        return applet.transmit(commandApdu);
    }

    protected BCECPublicKey sendGenerateKeysRequest() throws Exception {
        // Send instruction to the card to generate its keys
        ResponseAPDU response = sendCommandAPDU(new CommandAPDU(1234, InstructionCodes.GENERATE_KEYS, (byte) 0, (byte) 0, CertConstants.PUBLIC_KEY_LENGTH));

        // Parse, and return the public key of the card
        KeyPairGenerator generator = KeyPairGenerator.getInstance("ECDH", new BouncyCastleProvider());
        generator.initialize(ecSpec);
        byte[] rawPublicKeyCard = new byte[CertConstants.PUBLIC_KEY_LENGTH];
        System.arraycopy(response.getData(), 0, rawPublicKeyCard, 0, CertConstants.PUBLIC_KEY_LENGTH);
        ECPublicKeySpec keySpec = new ECPublicKeySpec(ecSpec.getCurve().decodePoint(rawPublicKeyCard), ecSpec);
        return new BCECPublicKey("ECDSA", keySpec, BouncyCastleProvider.CONFIGURATION);
    }

    protected void createAndSendCardCertificate(BCECPublicKey pubKeyCard) throws Exception{
        // Create a certificate for the card
        byte[] certificate = backend.generateCertificate(pubKeyCard, CertConstants.DEVICE_TYPE_CARD);

        // Send the certificate that the backend has generated to the card
        ResponseAPDU response = sendCommandAPDU(new CommandAPDU(1234, InstructionCodes.STORE_CERTIFICATE_AND_ISSUE, (byte) 0, (byte) 0, certificate, 0));
    }

    // Sends and receives data according to the Data Exchange protocol
    protected byte[] sendSecure(byte ins, byte[] data, short dataLe) throws Exception {
        // send R+1, data, MACk(R+1, data)

        // Increment the sequence number, and prepare data for MAC
        sequenceNumber = intToBytes(bytesToInt(sequenceNumber) + 1);
        byte[] dataToMac = combineArrays(sequenceNumber, data);
        byte[] paddedDataToMac = padTo16Bytes(dataToMac);

        byte[] mac = new byte[CertConstants.MAC_LENGTH];
        maccer.update(paddedDataToMac, 0, paddedDataToMac.length);
        maccer.doFinal(mac, 0);

        byte[] dataToSend = combineArrays(paddedDataToMac, mac);

        // Calculate expected return length when adding the sequence number, padding and MAC
        short le = calculateDataExchangeLe(dataLe);
        // Send and receive response
        ResponseAPDU cardResponse = sendIns(ins, dataToSend, le);
        byte[] responseData = cardResponse.getData();
        byte[] sequenceNumberCard = new byte[CertConstants.SEQUENCE_NUMBER_LENGTH];
        System.arraycopy(responseData, 0, sequenceNumberCard, 0, CertConstants.SEQUENCE_NUMBER_LENGTH);
        if (bytesToInt(sequenceNumberCard) != (bytesToInt(sequenceNumber) + 1)){
            print("Sequence number of response is invalid");
            throw new Exception("SequenceNumber of response is not valid");
        }
        print("Sequence number of response is valid!");

        if (!verifyMAC(
                responseData, (short) 0, (short) (responseData.length - CertConstants.MAC_LENGTH),
                responseData, (short) (responseData.length - CertConstants.MAC_LENGTH))){
            print("MAC of response is invalid");
            throw new Exception("Response MAC is not valid");
        }
        //print("MAC of response is valid!");
        byte[] realData = new byte[responseData.length - CertConstants.MAC_LENGTH - CertConstants.SEQUENCE_NUMBER_LENGTH];
        System.arraycopy(
                responseData, CertConstants.SEQUENCE_NUMBER_LENGTH,
                realData, 0,
                responseData.length - CertConstants.MAC_LENGTH - CertConstants.SEQUENCE_NUMBER_LENGTH
        );
        sequenceNumber = intToBytes(bytesToInt(sequenceNumber) + 1);

        // TODO: how about status codes? Or will we throw an exception if we get one of those
        print("Received data: " + toHexString(realData));
        return realData;
    }

    protected KeyPair getNewKeyPair() throws Exception {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("ECDH", new BouncyCastleProvider());
        generator.initialize(ecSpec);

        return generator.generateKeyPair();
    }

    protected byte[] certificateExchange() throws Exception {
        // Send Terminal Certificate to the Card
        ResponseAPDU responseCertificateExchange = sendIns(InstructionCodes.CERTIFICATE_EXCHANGE, terminalCert, (short) 160);

        // Store the certificate, and verify the signature and device type of the certificate
        byte[] responseData = responseCertificateExchange.getData();
        cardCert = new Cert(responseData, CertConstants.NONCE_LENGTH, (short)(responseData.length - CertConstants.NONCE_LENGTH));
        if (!cardCert.verifySignature(backend.getPublicKey()) || cardCert.getDeviceType() != CertConstants.DEVICE_TYPE_CARD){
            throw new Exception("Card certificate not valid");
        }

        // Retreive Nonce P
        byte[] p = new byte[CertConstants.NONCE_LENGTH];
        System.arraycopy(responseData,0, p, 0, CertConstants.NONCE_LENGTH);

        return p;
    }

    protected void mutualAuth(byte[] p) throws Exception {
        // Generate Nonce Q
        SecureRandom secureRandom = new SecureRandom();
        byte[] q = new byte[CertConstants.NONCE_LENGTH];
        secureRandom.nextBytes(q);

        // Sign [CardId, P]
        byte[] dataToSign = combineArrays(cardCert.getID(), p);
        PetrolCardSigner signer = SignerFactory.getSignerInstance(privateKey);
        byte[] dataToSend = combineArrays(q, signer.generateASN1Signature(dataToSign));

        // Send Q, [CardId, P] to the Card
        ResponseAPDU responseMutualAuth = sendIns(InstructionCodes.MUTUAL_AUTHENTICATION, dataToSend, (short) dataToSend.length);

        // Verify if Card sent [TerminalId, Q]
        byte[] deviceId = new byte[CertConstants.DEVICE_ID_LENGTH];
        System.arraycopy(terminalCert, CertConstants.OFFSET_DEVICE_ID, deviceId, 0, CertConstants.DEVICE_ID_LENGTH);
        byte[] dataToVerify = combineArrays(deviceId, q);

        PetrolCardVerifier verifier = SignerFactory.getVerifierInstance(cardCert.getPublicKeyBytes());

        if (!verifier.verifyASN1Signature(dataToVerify, responseMutualAuth.getData(), (short)0)) {
            throw new Exception("Verification of [TerminalId, nonce Q] failed!");
        }
        print("Successfully verified nonce Q");
    }

    // Establish session key using ECDH
    protected void sessionKeyExchange() throws Exception {
        // Generate session ec keypair
        KeyPair ephemeralKeyPair = getNewKeyPair();
        BCECPublicKey ephemeralPublicKey = (BCECPublicKey) ephemeralKeyPair.getPublic();
        BCECPrivateKey ephemeralPrivateKey = (BCECPrivateKey) ephemeralKeyPair.getPrivate();
        byte[] ephemeralPublicKeyBytes = ephemeralPublicKey.getQ().getEncoded(false);

        PetrolCardSigner signer = SignerFactory.getSignerInstance(privateKey);
        byte[] signature = signer.generateASN1Signature(ephemeralPublicKeyBytes);
        byte[] arrayToSend = combineArrays(ephemeralPublicKeyBytes, signature);

        // Send Terminal Ephemeral public key to the card
        ResponseAPDU responseKeyExchange = sendIns(InstructionCodes.ESTABLISH_SESSION_KEY, arrayToSend, (short) arrayToSend.length);

        // Get data from response APDU, and verify signature
        byte[] cardEphemeralPublicKey = new byte[CertConstants.PUBLIC_KEY_LENGTH];
        System.arraycopy(responseKeyExchange.getData(), 0, sequenceNumber, 0, CertConstants.SEQUENCE_NUMBER_LENGTH);
        System.arraycopy(responseKeyExchange.getData(), CertConstants.SEQUENCE_NUMBER_LENGTH, cardEphemeralPublicKey, 0, CertConstants.PUBLIC_KEY_LENGTH);
        PetrolCardVerifier verifier = SignerFactory.getVerifierInstance(cardCert.getPublicKeyBytes());

        if (!verifier.verifyASN1Signature(
                cardEphemeralPublicKey,
                responseKeyExchange.getData(),
                (short) (CertConstants.SEQUENCE_NUMBER_LENGTH + CertConstants.PUBLIC_KEY_LENGTH)
        )){
            throw new Exception("Signature of ephemeral public key of card is not valid.");
        }

        // Do the KeyAgreement to get the AES Key
        ECDHBasicAgreement keyAgreement = new ECDHBasicAgreement();
        ECPublicKeySpec cardEphemeralPKSpec = new ECPublicKeySpec(ecSpec.getCurve().decodePoint(cardEphemeralPublicKey), ecSpec);
        BCECPublicKey cardEphemeralPK = new BCECPublicKey("ECDSA", cardEphemeralPKSpec, BouncyCastleProvider.CONFIGURATION);

        ECDomainParameters ecParams = new ECDomainParameters(ecSpec.getCurve(), ecSpec.getG(), ecSpec.getN(), ecSpec.getH());
        org.bouncycastle.crypto.CipherParameters privateParam = new ECPrivateKeyParameters(ephemeralPrivateKey.getD(), ecParams);
        org.bouncycastle.crypto.CipherParameters publicParamCard = new ECPublicKeyParameters(cardEphemeralPK.getQ(), ecParams);
        keyAgreement.init(privateParam);
        BigInteger aesKey = keyAgreement.calculateAgreement(publicParamCard);

        // Hash the output of the keyAgreement, and store result AES SessionKey
        System.arraycopy(MessageDigest.getInstance("SHA-1").digest(aesKey.toByteArray()), 0, sessionKey, 0, sessionKey.length);

        // Init MACcer with new AES key
        maccer.init(new KeyParameter(sessionKey)); 

        print("Sequence Number: " + toHexString(sequenceNumber));
        print("AES KEY: " + toHexString(sessionKey));
    }

    protected ResponseAPDU sendIns(byte ins, byte[] data) throws CardException {
        return applet.transmit(new CommandAPDU(0, ins, 0, 0, data));
    }

    protected ResponseAPDU sendIns(byte ins, byte[] data, short le) throws CardException {
        return applet.transmit(new CommandAPDU(0, ins, 0, 0, data, le));
    }

    // Pads data to the next 16 bytes if not a multiple of 16 already
    protected byte[] padTo16Bytes(byte[] data)
    {
        int a = data.length;
        int b = (int) Math.round(Math.ceil(a/16.0)) * 16;

        //print("Data padding: Old length = " + a + " New length = " + b );

        byte[] paddedData = new byte[b];
        System.arraycopy(data, 0, paddedData, 0, a);
        return paddedData;
    }

    // Verifies a MAC against the original data, since no built-in function for this exists
    protected boolean verifyMAC(byte[] data, short dataOffset, short dataLength, byte[] mac, short macOffset){
        byte[] result = new byte[CertConstants.MAC_LENGTH];
        maccer.update(data, dataOffset, dataLength);
        maccer.doFinal(result, 0); // FIY: doFinal resets the maccer state

        return Arrays.equals(result, 0, CertConstants.MAC_LENGTH, mac, macOffset, CertConstants.MAC_LENGTH + macOffset);
    }

    protected short calculateDataExchangeLe(short dataLe){
        short le = (short) (dataLe + CertConstants.SEQUENCE_NUMBER_LENGTH); // sequence number
        le = (short) (Math.round(Math.ceil(le/16.0)) * 16); // padding
        return (short) ((le + CertConstants.MAC_LENGTH)); // mac
    }

    protected void checkCardExpiryDate() throws Exception{
        //use cardCert
        long expDate = cardCert.getExpiryDate();
        print("ExpDate of card: " + expDate);
        //Date expiry = new Date(expDate * 1000); // Can be used to pretty print the date
        long now = (System.currentTimeMillis() / 1000L);
        print("Current date: " + now);
        if (expDate < now){
            throw new Exception("Expiry date of card invalid");
        }
        print("Card expiry date is valid");
    }

    protected void sendLogsToBackend(boolean backendOnline){
        print("Connecting to backend");
        if (backendOnline){
            print("Connected to backend");
        }
        else{
            print("No connection to backend, abort");
            return;
        }
        print("Authenticated with backend (out of scope)");
        String toSendString = "|" + terminalName + "#" + deviceId + ":" + String.join("|" + terminalName + "#" + deviceId + ":", logs);
        backend.writeLog(toSendString);
        print("Logs sent to backend, clearing local logs");
        logs = new ArrayList<>();
    }

    // Requests and prints all logs of the card
    protected void readLogs() throws Exception{
        byte availableLogs = CertConstants.CARD_LOG_SIZE / CertConstants.SINGLE_LOG_SIZE;
        for (byte i = 0; i < availableLogs; i++)
        {
            print("Requesting log " + i);

            byte[] response = sendSecure(InstructionCodes.REQUEST_LOGS, new byte[]{i}, CertConstants.SINGLE_LOG_SIZE);
            print("Response for log " + i + ": " + toHexString(response));
        }

    }

    protected byte[] prependToArray(byte item, byte[] array) {
        byte[] data = new byte[array.length + 1 ];
        System.arraycopy(array, 0, data, 1, array.length);
        data[0] = item;
        return data;
    }

    protected byte[] appendToArray(byte[] array, byte item) {
        byte[] data = new byte[array.length + 1 ];
        System.arraycopy(array, 0, data, 0, array.length);
        data[array.length] = item;
        return data;
    }

    protected byte[] combineArrays(byte[] array1, byte[] array2) {
        byte[] data = new byte[array1.length + array2.length ];
        System.arraycopy(array1, 0, data, 0, array1.length);
        System.arraycopy(array2, 0, data, array1.length, array2.length);
        return data;
    }

    protected byte[] ensureNbyteLengthArray(byte[] input, int length){
        byte[] output = new byte[length];
        System.arraycopy(input, 0, output, length-input.length, input.length);
        return output;
    }

    protected void print(String stringToPrint) {
        System.out.println(String.format("[%s]: ", terminalName.toUpperCase(Locale.ROOT)) + stringToPrint);
    }

    public static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b: bytes) {
            sb.append(String.format("%02X ", b));
        }
        return sb.toString();
    }

    public static byte[] intToBytes(int i) {
        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.putInt(i); 
        return bb.array();
    }

    public static byte[] shortToBytes(short i) {
        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.putShort(i);
        return bb.array();
    }

    public static byte[] longToBytes(long i){
        ByteBuffer bb = ByteBuffer.allocate(8);
        bb.putLong(i);
        return bb.array();
    }

    public static int bytesToInt(byte[] bytes){
        return ByteBuffer.wrap(bytes).getInt();
    }

    public static int bytesToShort(byte[] bytes){
        return ByteBuffer.wrap(bytes).getShort();
    }
}
